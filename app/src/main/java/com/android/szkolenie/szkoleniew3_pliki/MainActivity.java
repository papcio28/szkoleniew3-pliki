package com.android.szkolenie.szkoleniew3_pliki;

import android.os.Environment;
import android.support.v4.os.EnvironmentCompat;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;


public class MainActivity extends ActionBarActivity {

    public static final String FILENAME = "szkolenieW3.txt";

    @InjectView(R.id.pole_edycji)   // Zamiast findViewById
    protected EditText mPoleTekstowe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Wstrzyknięcie w pola obiektów widoków
        // oraz przypięcie onClickListenerów
        ButterKnife.inject(this);   // To musi być !
    }

    @OnClick(R.id.btn_save_internal)    // Zamiast mButton.setOnClickListener
    public void zapiszInternal() {
        // Ścieżka do katalogu aplikacji
        // Nie musimy sprawdzać czy jest do zapisu/odczytu
        File mFilesDir = getFilesDir(); // Wskazuje na folder !
        File mPlikTekstowy = new File(mFilesDir, FILENAME);

        writeToFile(mPlikTekstowy);
    }

    // Zwraca informacje czy mozna zapisywac do External Storage
    private boolean isExternalStorageWritable() {
        String mExternalState = Environment.getExternalStorageState();
        return mExternalState.equals(Environment.MEDIA_MOUNTED);
    }

    private void writeToFile(File mPlikTekstowy) {
        try {
            // Tworzymy strumień wyjścia do pliku
            FileOutputStream mFileOutput = new FileOutputStream(mPlikTekstowy);

            // Przepisujemy tekst z pola edycyjnego do strumienia pliku
            IOUtils.write(mPoleTekstowe.getText().toString(), mFileOutput);

            // Zamykamy strumień
            mFileOutput.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void readFromFile(File mPlikTekstowy) {
        if(mPlikTekstowy.exists()) {
            // Odczyt z pliku
            try {
                // Otwarcie strumienia wejściowego do pliku
                FileInputStream mFileInput = new FileInputStream(mPlikTekstowy);
                // Odczytanie strumienia z pliku do zmiennej typu String
                String mTekst = IOUtils.toString(mFileInput);

                // Wpisanie odczytanego tekstu do pola tekstowego
                mPoleTekstowe.setText(mTekst);

                // Zamknięcie strumienia
                mFileInput.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            // Informacja o braku pliku !
            Toast.makeText(this,
                    "Nie znaleziono pliku !", Toast.LENGTH_LONG).show();
        }
    }

    @OnClick(R.id.btn_save_external)
    public void zapiszExternal() {
        // Sprawdzamy czy External Storage jest zapisywalne !
        if(isExternalStorageWritable()) {
            File mExternalDir =
                    Environment.getExternalStoragePublicDirectory(
                            Environment.DIRECTORY_DOWNLOADS);
            File mAppDir = new File(mExternalDir, "szkolenieW3");
            // Założenie katalogów, których brakuje
            if(!mAppDir.exists()) { mAppDir.mkdirs(); }

            File mTargetFile = new File(mAppDir, FILENAME);

            Log.d("FileSave", mTargetFile.getAbsolutePath());

            writeToFile(mTargetFile);
        } else {
            Toast.makeText(this,
                    "External Storage niedostępne !", Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.btn_read_internal)
    public void odczytajInternal() {
        File mFilesDir = getFilesDir();
        File mPlikTekstowy = new File(mFilesDir, FILENAME);

        readFromFile(mPlikTekstowy);
    }

    @OnClick(R.id.btn_read_external)
    public void odczytajExternal() {
        if(isExternalStorageWritable()) {
            File mExternalDir =
                    Environment.getExternalStoragePublicDirectory(
                            Environment.DIRECTORY_DOWNLOADS);
            File mAppDir = new File(mExternalDir, "szkolenieW3");
            // Założenie katalogów, których brakuje
            if(!mAppDir.exists()) { mAppDir.mkdirs(); }

            File mTargetFile = new File(mAppDir, FILENAME);

            readFromFile(mTargetFile);
        } else {
            Toast.makeText(this,
                    "External Storage niedostępne !", Toast.LENGTH_SHORT).show();
        }
    }
}
